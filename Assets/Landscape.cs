﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Landscape : MonoBehaviour
{
    public ScreenOrientation orientation = ScreenOrientation.Landscape;

    // Start is called before the first frame update
    void Start()
    {
        Screen.orientation = orientation;
    }
   

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AboutClick()
    {
    }

    public void ExitClick()
    {
    }

    public void HowClick()
    {
    }

    public void ScanClick()
    {
        SceneManager.LoadScene("testscene", LoadSceneMode.Additive);
    }

}
