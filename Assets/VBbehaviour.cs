﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class VBbehaviour : MonoBehaviour, IVirtualButtonEventHandler {
    public GameObject vbtn;
    public GameObject Detail;
	// Use this for initialization
	void Start () {
        vbtn = GameObject.Find("detail");
        vbtn.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);

        vbtn = GameObject.Find("Detail");
        vbtn.GetComponent<Sprite>();
    }
    public void OnButtonPressed(VirtualButtonBehaviour vb) {
        Detail.SetActive(true);
        Debug.Log("pressed");
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb) {
        Detail.SetActive(false);
        Debug.Log("released");
    }
    // Update is called once per frame
    void Update () {
		
	}
}
