﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public static GameController instance;
    public Button Button, ButtonInfo;
    public GameObject Menu;
    public GameObject Canvas;
    public GameObject Detail;
    public Button m_YourFirstButton, m_YourSecondButton;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    // Use this for initialization
    void Start () {
        Menu.SetActive(true);
        Canvas.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
       // m_YourFirstButton.onClick.AddListener(TaskOnClick);
    } 

    public

    void GameStart()
    {
        Menu.SetActive(false);
        Canvas.SetActive(true);
    }

    void DetailAnimal()
    {
        Menu.SetActive(false);
        Canvas.SetActive(false);
        Detail.SetActive(true);
    }

}

