﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menu : MonoBehaviour{

    public static menu instance;
    public GameObject ScanNow;
    public GameObject Menu;
    public GameObject Canvas;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        Menu.SetActive(true);
        Canvas.SetActive(false);
    }


    void Update()
    {
            for (var i = 0; i < Input.touchCount; ++i)
            {
                if (Input.GetTouch(i).phase == TouchPhase.Began || Input.GetTouch(i).phase == TouchPhase.Stationary)
                {
                    RaycastHit2D hitInfo = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.GetTouch(i).position), Vector2.zero);
                    // RaycastHit2D can be either true or null, but has an implicit conversion to bool, so we can use it like this
                    if (hitInfo)
                    {
                        Debug.Log(hitInfo.transform.gameObject.name);
                        if (hitInfo.transform.gameObject.name == "ButtonScan")
                        {
                            GameStart();
                            Debug.Log("Touched");
                        }
                        else if (hitInfo.transform.gameObject.name == "ButtonInfo")
                        {
                            DetailAnimal();
                            Debug.Log("Touched");
                        }
                    }
                }
        }
    }

    public

    void GameStart()
    {
        Menu.SetActive(false);
        Canvas.SetActive(true);
    }

    void DetailAnimal()
    {
        Menu.SetActive(false);
        Canvas.SetActive(false);
    }

}